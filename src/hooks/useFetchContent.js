import {useCallback, useState} from "react";

export const API_URL = 'https://rickandmortyapi.com/api/character',
    API_SEARCH_URL = API_URL + '/?name=',
    API_LIMIT = 10

export let characters = []

export const useFetchContent = () => {
    let charactersVisible = []

    const [imgList, setImgList] = useState([]),

        fetch = useCallback(async (searchValue = '') => {
            characters = await fetchCharacter(searchValue) ?? [];
            charactersVisible = [...characters.splice(0, API_LIMIT)]
            setImgList(charactersVisible)
        }, []),

        fetchMore = useCallback(async () => {
            charactersVisible = [...charactersVisible, ...characters.splice(0, API_LIMIT)]
            setImgList(charactersVisible)
        }, []);

    return [imgList, fetch, fetchMore];
};

const fetchCharacter = async (searchValue) => {
    const response = await fetch(searchValue !== '' ? API_SEARCH_URL + searchValue : API_URL),
        data = await response.json()

    return data.results ?? []
}
