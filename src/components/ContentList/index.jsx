import React from "react"
import "./index.css";

const ContentList = ({content}) => {
    return (
        <ul>
            {content.map(elem => (
                <li key={elem.id} className="Elem">
                    <img className="Img" src={elem.image} alt={elem.name} id={elem.id}/>
                    <div className="Name">{elem.name}</div>
                </li>
            ))}
        </ul>
    );
};

export default ContentList;
