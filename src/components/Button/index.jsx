import React from "react"
import "./index.css";

const Button = (props) => (
  <button className="Button" {...props} />
);

export default Button;
