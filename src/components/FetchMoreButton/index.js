import React from "react"
import "./index.css";
import {characters} from "../../hooks/useFetchContent";

function FetchMoreButton({fetchMore}) {
    if (characters.length > 0) {
        return (
            <button className="FetchButton" onClick={fetchMore}>Load more</button>
        )
    } else return null

}

export default FetchMoreButton